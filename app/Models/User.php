<?php

namespace App\Models;

use App\Events\User\RequestedPasswordResetEmail;
use App\Mail\ResetPassword;
use App\Presenters\Traits\Presentable;
use App\Presenters\UserPresenter;
use App\Services\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;
use App\Services\Auth\TwoFactor\Contracts\Authenticatable as TwoFactorAuthenticatableContract;
use App\Support\Authorization\AuthorizationUserTrait;
use App\Support\Enum\UserStatus;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Mail;

class User extends Authenticatable implements TwoFactorAuthenticatableContract, MustVerifyEmail
{
    use AuthorizationUserTrait;
    use CanResetPassword;
    use HasApiTokens;
    use Notifiable;
    use Presentable;
    use TwoFactorAuthenticatable;

    protected $presenter = UserPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['last_login', 'birthday'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'username', 'first_name', 'last_name', 'phone', 'avatar',
        'address', 'country_id', 'birthday', 'last_login', 'confirmation_token', 'status',
        'remember_token', 'role_id', 'email_verified_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Always encrypt password when it is updated.
     *
     * @param $value
     *
     * @return string
     */
    public function setPasswordAttribute($value): ?string
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = trim($value) ?: null;
    }

    public function gravatar()
    {
        $hash = hash('md5', strtolower(trim($this->attributes['email'])));

        return sprintf('https://www.gravatar.com/avatar/%s?size=150', $hash);
    }

    public function isUnconfirmed(): bool
    {
        return UserStatus::UNCONFIRMED == $this->status;
    }

    public function isActive(): bool
    {
        return UserStatus::ACTIVE == $this->status;
    }

    public function isBanned(): bool
    {
        return UserStatus::BANNED == $this->status;
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this)->send(new ResetPassword($token));

        event(new RequestedPasswordResetEmail($this));
    }
}

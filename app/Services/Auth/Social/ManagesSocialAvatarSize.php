<?php

namespace App\Services\Auth\Social;

use Laravel\Socialite\Contracts\User as SocialUser;

trait ManagesSocialAvatarSize
{
    /**
     * Get appropriate image size for a specific provider.
     *
     * @param $provider
     *
     * @return mixed|string
     */
    protected function getAvatarForProvider($provider, SocialUser $socialUser)
    {
        if ('facebook' == $provider) {
            return str_replace('width=1920', 'width=150', $socialUser->avatar_original);
        }

        if ('google' == $provider) {
            return $socialUser->avatar_original . '?sz=150';
        }

        if ('twitter' == $provider) {
            return str_replace('_normal', '_200x200', $socialUser->getAvatar());
        }

        return $socialUser->getAvatar();
    }
}

<?php

namespace App\Presenters\Traits;

trait Presentable
{
    /**
     * @var \App\Presenters\Presenter
     */
    protected $presenterInstance;

    /**
     * @throws \Exception
     */
    public function present()
    {
        if (\is_object($this->presenterInstance)) {
            return $this->presenterInstance;
        }
        if (property_exists($this, 'presenter') && class_exists($this->presenter)) {
            return $this->presenterInstance = new $this->presenter($this);
        }
        throw new \Exception('Property $presenter was not set correctly in ' . static::class);
    }
}

<?php

namespace App\Support\Authorization;

use App\Models\Permission;
use Cache;
use Config;

trait AuthorizationRoleTrait
{
    /**
     * Override "save" role method to clear role cache.
     */
    public function save(array $options = []): void
    {
        $this->flushCache();
        parent::save($options);
    }

    /**
     * Override "delete" role method to clear role cache.
     *
     * @throws \Exception
     */
    public function delete(array $options = []): void
    {
        $this->flushCache();
        parent::delete($options);
    }

    /**
     * Override "restore" role method to clear role cache.
     */
    public function restore(): void
    {
        $this->flushCache();
        parent::restore();
    }

    /**
     * Checks if the role has a permission by its name.
     *
     * @param string $name Permission name.
     */
    public function hasPermission(string $name): bool
    {
        $perms = $this->cachedPermissions()->pluck('name')->toArray();

        return \in_array($name, $perms, true);
    }

    /**
     * Get cached permissions for this role.
     */
    public function cachedPermissions()
    {
        return Cache::remember($this->getCacheKey(), Config::get('cache.ttl'), function () {
            return $this->permissions()->get();
        });
    }

    /**
     * Many-to-Many relations with the permission model.
     */
    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_id');
    }

    /**
     * Save the inputted permissions.
     */
    public function savePermissions($inputPermissions): void
    {
        if (!empty($inputPermissions)) {
            $this->permissions()->sync($inputPermissions);
        } else {
            $this->permissions()->detach();
        }

        $this->flushCache();
    }

    /**
     * Attach multiple permissions to current role.
     */
    public function attachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->attachPermission($permission);
        }
    }

    /**
     * Attach permission to current role.
     *
     * @param array|object $permission
     */
    public function attachPermission($permission): void
    {
        if (\is_object($permission)) {
            $permission = $permission->getKey();
        }

        if (\is_array($permission)) {
            $permission = $permission['id'];
        }

        $this->permissions()->attach($permission);

        $this->flushCache();
    }

    /**
     * Detach multiple permissions from current role.
     */
    public function detachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->detachPermission($permission);
        }
    }

    /**
     * Detach permission from current role.
     *
     * @param array|object $permission
     */
    public function detachPermission($permission): void
    {
        if (\is_object($permission)) {
            $permission = $permission->getKey();
        }

        if (\is_array($permission)) {
            $permission = $permission['id'];
        }

        $this->permissions()->detach($permission);

        $this->flushCache();
    }

    /**
     * Sync role permissions.
     *
     * @param $permissions array Permission IDs.
     */
    public function syncPermissions(array $permissions): void
    {
        $this->permissions()->sync($permissions);

        $this->flushCache();
    }

    /**
     * Flush cached permissions for this role.
     */
    private function flushCache(): void
    {
        Cache::forget($this->getCacheKey());
    }

    /**
     * Get permissions cache key.
     */
    private function getCacheKey(): string
    {
        return 'permissions_for_role_' . $this->{$this->primaryKey};
    }
}

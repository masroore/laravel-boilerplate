<?php

namespace App\Repositories\User;

use App\Models\User;
use Carbon\Carbon;
use Laravel\Socialite\Contracts\User as SocialUser;

interface UserRepository
{
    /**
     * Paginate registered users.
     *
     * @param $perPage
     * @param null $search
     * @param null $status
     */
    public function paginate($perPage, $search = null, $status = null);

    /**
     * Find user by its id.
     *
     * @param $id
     *
     * @return User|null
     */
    public function find($id);

    /**
     * Find user by email.
     *
     * @param $email
     *
     * @return User|null
     */
    public function findByEmail($email);

    /**
     * Find user registered via social network.
     *
     * @param $provider string Provider used for authentication.
     * @param $providerId string Provider's unique identifier for authenticated user.
     */
    public function findBySocialId($provider, $providerId);

    /**
     * Find user by specified session id.
     *
     * @param $sessionId
     */
    public function findBySessionId($sessionId);

    /**
     * Create new user.
     */
    public function create(array $data);

    /**
     * Update user specified by it's id.
     *
     * @param $id
     *
     * @return User
     */
    public function update($id, array $data);

    /**
     * Delete user with provided id.
     *
     * @param $id
     */
    public function delete($id);

    /**
     * Associate account details returned from social network
     * to user with provided user id.
     *
     * @param $userId
     * @param $provider
     */
    public function associateSocialAccountForUser($userId, $provider, SocialUser $user);

    /**
     * Number of users in database.
     */
    public function count();

    /**
     * Number of users registered during current month.
     */
    public function newUsersCount();

    /**
     * Number of users with provided status.
     *
     * @param $status
     */
    public function countByStatus($status);

    /**
     * Count of registered users for every month within the
     * provided date range.
     *
     * @param $from
     * @param $to
     */
    public function countOfNewUsersPerMonth(Carbon $from, Carbon $to);

    /**
     * Get latest {$count} users from database.
     *
     * @param $count
     */
    public function latest($count = 20);

    /**
     * Set specified role to specified user.
     *
     * @param $userId
     * @param $roleId
     */
    public function setRole($userId, $roleId);

    /**
     * Change role for all users who has role $fromRoleId to $toRoleId.
     *
     * @param $fromRoleId Id of current role.
     * @param $toRoleId Id of new role.
     */
    public function switchRolesForUsers($fromRoleId, $toRoleId);

    /**
     * Get all users with provided role.
     *
     * @param $roleName
     */
    public function getUsersWithRole($roleName);

    /**
     * Get all social login records for specified user.
     *
     * @param $userId
     */
    public function getUserSocialLogins($userId);

    /**
     * Find user by confirmation token.
     *
     * @param $token
     */
    public function findByConfirmationToken($token);
}

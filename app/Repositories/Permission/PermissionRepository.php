<?php

namespace App\Repositories\Permission;

interface PermissionRepository
{
    /**
     * Get all system permissions.
     */
    public function all();

    /**
     * Finds the permission by given id.
     *
     * @param $id
     */
    public function find($id);

    /**
     * Creates new permission from provided data.
     */
    public function create(array $data);

    /**
     * Updates specified permission.
     *
     * @param $id
     */
    public function update($id, array $data);

    /**
     * Remove specified permission from repository.
     *
     * @param $id
     */
    public function delete($id);
}

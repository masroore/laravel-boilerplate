<?php

namespace App\Repositories\Role;

use App\Events\Role\Created;
use App\Events\Role\Deleted;
use App\Events\Role\Updated;
use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

class EloquentRole implements RoleRepository
{
    public function all(): Collection
    {
        return Role::all();
    }

    public function getAllWithUsersCount()
    {
        return Role::withCount('users')->get();
    }

    public function create(array $data): Role
    {
        $role = Role::create($data);

        event(new Created($role));

        return $role;
    }

    public function update($id, array $data): Role
    {
        $role = $this->find($id);

        $role->update($data);

        event(new Updated($role));

        return $role;
    }

    public function find($id): Role
    {
        return Role::find($id);
    }

    public function delete($id): bool
    {
        $role = $this->find($id);

        event(new Deleted($role));

        return $role->delete();
    }

    public function updatePermissions($roleId, array $permissions)
    {
        $role = $this->find($roleId);

        $role->syncPermissions($permissions);
    }

    public function lists($column = 'name', $key = 'id')
    {
        return Role::pluck($column, $key);
    }

    public function findByName($name): Role
    {
        return Role::where('name', $name)->first();
    }
}

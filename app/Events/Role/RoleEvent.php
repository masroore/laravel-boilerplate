<?php

namespace App\Events\Role;

use App\Models\Role;

abstract class RoleEvent
{
    protected Role $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}

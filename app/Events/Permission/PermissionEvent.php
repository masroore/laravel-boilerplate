<?php

namespace App\Events\Permission;

use App\Models\Permission;

abstract class PermissionEvent
{
    protected Permission $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function getPermission(): Permission
    {
        return $this->permission;
    }
}

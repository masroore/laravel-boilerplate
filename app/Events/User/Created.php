<?php

namespace App\Events\User;

use App\Models\User;

class Created
{
    protected User $createdUser;

    public function __construct(User $createdUser)
    {
        $this->createdUser = $createdUser;
    }

    public function getCreatedUser(): User
    {
        return $this->createdUser;
    }
}

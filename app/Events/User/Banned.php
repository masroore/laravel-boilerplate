<?php

namespace App\Events\User;

use App\Models\User;

class Banned
{
    protected User $bannedUser;

    public function __construct(User $bannedUser)
    {
        $this->bannedUser = $bannedUser;
    }

    public function getBannedUser(): User
    {
        return $this->bannedUser;
    }
}

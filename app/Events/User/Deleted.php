<?php

namespace App\Events\User;

use App\Models\User;

class Deleted
{
    protected User $deletedUser;

    public function __construct(User $deletedUser)
    {
        $this->deletedUser = $deletedUser;
    }

    public function getDeletedUser(): User
    {
        return $this->deletedUser;
    }
}

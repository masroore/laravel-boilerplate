<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DatabaseSession
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function handle($request, Closure $next)
    {
        // If we are not using database session driver,
        // just display 404 page
        if ('database' != config('session.driver')) {
            throw new NotFoundHttpException('The entity you are looking for does not exist.');
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Requests\User;

class UpdateProfileLoginDetailsRequest extends UpdateLoginDetailsRequest
{
    /**
     * Get authenticated user.
     */
    protected function getUserForUpdate()
    {
        return \Auth::user();
    }
}

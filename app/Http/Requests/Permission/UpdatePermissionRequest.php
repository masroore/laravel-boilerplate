<?php

namespace App\Http\Requests\Permission;

use App\Rules\ValidPermissionName;
use Illuminate\Validation\Rule;

class UpdatePermissionRequest extends BasePermissionRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                new ValidPermissionName(),
                Rule::unique('permissions', 'name')->ignore($this->route('permission')->id),
            ],
        ];
    }
}

<?php

namespace App\Http\Requests\Auth\Social;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class ApiAuthenticateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'network' => [
                'required',
                Rule::in(config('auth.social.providers')),
            ],
            'social_token' => 'required',
            'device_name' => 'required',
        ];
    }
}

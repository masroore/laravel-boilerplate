<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\QueryBuilder\AllowedInclude;

class RoleResource extends JsonResource
{
    public static function allowedIncludes()
    {
        return [
            'permissions',
            AllowedInclude::count('users_count'),
        ];
    }

    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array
    {
        return [
            'id' => (int) $this->id,
            'name' => $this->name,
            'display_name' => $this->display_name,
            'description' => $this->description,
            'removable' => (bool) $this->removable,
            'users_count' => null === $this->users_count ? null : (int) $this->users_count,
            'updated_at' => (string) $this->updated_at,
            'created_at' => (string) $this->created_at,
            'permissions' => PermissionResource::collection($this->whenLoaded('permissions')),
        ];
    }
}

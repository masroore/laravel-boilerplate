<?php

namespace App\Http\Controllers\Auth\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\Role\RoleRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    private UserRepository $users;

    /**
     * Create a new authentication controller instance.
     */
    public function __construct(UserRepository $users)
    {
        $this->middleware('registration')->only('show', 'register');

        $this->users = $users;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('auth.register', [
            'socialProviders' => config('auth.social.providers'),
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request, RoleRepository $roles)
    {
        $user = $this->users->create(
            array_merge($request->validFormData(), ['role_id' => $roles->findByName('User')->id])
        );

        event(new Registered($user));

        $message = config('autah.confirm_email')
            ? __('Your account is created successfully! Please confirm your email.')
            : __('Your account is created successfully!');

        \Auth::login($user);

        return redirect('/')->with('success', $message);
    }
}

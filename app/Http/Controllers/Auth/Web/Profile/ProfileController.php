<?php

namespace App\Http\Controllers\Auth\Web\Profile;

use App\Http\Controllers\Controller;
use App\Repositories\Country\CountryRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    private UserRepository $users;

    private RoleRepository $roles;

    private CountryRepository $countries;

    public function __construct(UserRepository $users, RoleRepository $roles, CountryRepository $countries)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->countries = $countries;
    }

    /**
     * Display user's profile page.
     *
     * @return Factory|View
     */
    public function show()
    {
        $roles = $this->roles->all()->filter(function ($role) {
            return $role->id == auth()->user()->role_id;
        })->pluck('name', 'id');

        return view('user.profile', [
            'user' => auth()->user(),
            'edit' => true,
            'roles' => $roles,
            'countries' => [0 => __('Select a Country')] + $this->countries->lists()->toArray(),
            'socialLogins' => $this->users->getUserSocialLogins(auth()->id()),
            'statuses' => UserStatus::lists(),
        ]);
    }
}

<?php

namespace App\Http\Controllers\Auth\Web\Profile;

use App\Events\User\UpdatedProfileDetails;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateProfileDetailsRequest;
use App\Repositories\User\UserRepository;

/**
 * Class DetailsController.
 */
class DetailsController extends Controller
{
    private UserRepository $users;

    /**
     * DetailsController constructor.
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Update profile details.
     */
    public function update(UpdateProfileDetailsRequest $request)
    {
        $this->users->update(auth()->id(), $request->except('role_id', 'status'));

        event(new UpdatedProfileDetails());

        return redirect()->back()
            ->withSuccess(__('Profile updated successfully.'));
    }
}

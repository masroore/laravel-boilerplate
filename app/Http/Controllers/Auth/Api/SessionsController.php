<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Resources\SessionResource;
use App\Repositories\Session\SessionRepository;

/**
 * Class SessionsController.
 */
class SessionsController extends ApiController
{
    private SessionRepository $sessions;

    public function __construct(SessionRepository $sessions)
    {
        $this->middleware('session.database');
        $this->sessions = $sessions;
    }

    /**
     * Get info about specified session.
     *
     * @param $session
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return SessionResource
     */
    public function show($session)
    {
        $this->authorize('manage-session', $session);

        return new SessionResource($session);
    }

    /**
     * Destroy specified session.
     *
     * @param $session
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($session)
    {
        $this->authorize('manage-session', $session);

        $this->sessions->invalidateSession($session->id);

        return $this->respondWithSuccess();
    }
}

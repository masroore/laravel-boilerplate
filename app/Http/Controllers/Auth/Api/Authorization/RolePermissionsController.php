<?php

namespace App\Http\Controllers\Auth\Api\Authorization;

use App\Events\Role\PermissionsUpdated;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Role\UpdateRolePermissionsRequest;
use App\Http\Resources\PermissionResource;
use App\Models\Role;
use App\Repositories\Role\RoleRepository;

class RolePermissionsController extends ApiController
{
    private RoleRepository $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
        $this->middleware('permission:permissions.manage');
    }

    public function show(Role $role)
    {
        return PermissionResource::collection($role->cachedPermissions());
    }

    /**
     * Update specified role.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function update(Role $role, UpdateRolePermissionsRequest $request)
    {
        $this->roles->updatePermissions(
            $role->id,
            $request->permissions
        );

        event(new PermissionsUpdated());

        return PermissionResource::collection($role->cachedPermissions());
    }
}

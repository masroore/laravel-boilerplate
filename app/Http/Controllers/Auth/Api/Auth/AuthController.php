<?php

namespace App\Http\Controllers\Auth\Api\Auth;

use App\Events\User\LoggedIn;
use App\Events\User\LoggedOut;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Auth\ApiLoginRequest;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Class LoginController.
 */
class AuthController extends ApiController
{
    public function __construct()
    {
        $this->middleware('guest')->only('login');
        $this->middleware('auth')->only('logout');
    }

    /**
     * Attempt to log the user in and generate unique
     * JWT token on successful authentication.
     *
     * @throws ValidationException
     * @throws BindingResolutionException
     *
     * @return JsonResponse|Response
     */
    public function token(ApiLoginRequest $request)
    {
        $user = $this->findUser($request);

        if ($user->isBanned()) {
            return $this->errorUnauthorized(__('Your account is banned by administrators.'));
        }

        Auth::setUser($user);

        event(new LoggedIn());

        return $this->respondWithArray([
            'token' => $user->createToken($request->device_name)->plainTextToken,
        ]);
    }

    /**
     * Logout user and invalidate token.
     *
     * @return JsonResponse
     */
    public function logout()
    {
        event(new LoggedOut());

        auth()->user()->currentAccessToken()->delete();

        return $this->respondWithSuccess();
    }

    /**
     * Find the user instance from the API request.
     *
     * @throws BindingResolutionException
     * @throws ValidationException
     */
    private function findUser(ApiLoginRequest $request): User
    {
        $user = User::where($request->getCredentials())->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages(['username' => [trans('auth.failed')]]);
        }

        return $user;
    }
}

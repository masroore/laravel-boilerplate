<?php

namespace App\Http\Controllers\Auth\Api\Auth;

use App\Events\User\LoggedIn;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Auth\Social\ApiAuthenticateRequest;
use App\Repositories\User\UserRepository;
use App\Services\Auth\Social\SocialManager;
use Auth;
use Exception;
use Socialite;

class SocialLoginController extends ApiController
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var SocialManager
     */
    private $socialManager;

    public function __construct(UserRepository $users, SocialManager $socialManager)
    {
        $this->users = $users;
        $this->socialManager = $socialManager;
    }

    public function index(ApiAuthenticateRequest $request)
    {
        try {
            $socialUser = Socialite::driver($request->network)->userFromToken($request->social_token);
        } catch (Exception $e) {
            return $this->errorInternalError('Could not connect to specified social network.');
        }

        $user = $this->users->findBySocialId(
            $request->network,
            $socialUser->getId()
        );

        if (!$user) {
            if (!setting('reg_enabled')) {
                return $this->errorForbidden('Only users who already created an account can log in.');
            }

            $user = $this->socialManager->associate($socialUser, $request->network);
        }

        if ($user->isBanned()) {
            return $this->errorForbidden(__('Your account is banned by administrators.'));
        }

        Auth::setUser($user);

        event(new LoggedIn());

        return $this->respondWithArray([
            'token' => $user->createToken($request->device_name)->plainTextToken,
        ]);
    }
}

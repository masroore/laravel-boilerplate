<?php

namespace App\Http\Controllers\Auth\Api\Auth\Password;

use App\Events\User\RequestedPasswordResetEmail;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Auth\PasswordRemindRequest;
use App\Mail\ResetPassword;
use App\Repositories\User\UserRepository;
use Illuminate\Http\JsonResponse;
use Mail;
use Password;

class RemindController extends ApiController
{
    /**
     * Send a reset link to the given user.
     *
     * @return JsonResponse
     */
    public function index(PasswordRemindRequest $request, UserRepository $users)
    {
        $user = $users->findByEmail($request->email);

        $token = Password::getRepository()->create($user);

        Mail::to($user)->send(new ResetPassword($token));

        event(new RequestedPasswordResetEmail($user));

        return $this->respondWithSuccess();
    }
}

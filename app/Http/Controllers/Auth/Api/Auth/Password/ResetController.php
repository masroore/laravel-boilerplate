<?php

namespace App\Http\Controllers\Auth\Api\Auth\Password;

use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Auth\PasswordResetRequest;
use Illuminate\Auth\Events\PasswordReset;
use Password;

class ResetController extends ApiController
{
    /**
     * Reset the given user's password.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(PasswordResetRequest $request)
    {
        $response = Password::reset($request->credentials(), function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->respondWithSuccess();

            default:
                return $this->setStatusCode(400)
                    ->respondWithError(trans($response));
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param string                                      $password
     *
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;
        $user->save();

        event(new PasswordReset($user));
    }
}

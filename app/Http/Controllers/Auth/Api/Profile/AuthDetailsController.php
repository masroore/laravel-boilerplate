<?php

namespace App\Http\Controllers\Auth\Api\Profile;

use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\User\UpdateProfileLoginDetailsRequest;
use App\Http\Resources\UserResource;
use App\Repositories\User\UserRepository;

class AuthDetailsController extends ApiController
{
    /**
     * Updates user profile details.
     *
     * @return UserResource
     */
    public function update(UpdateProfileLoginDetailsRequest $request, UserRepository $users)
    {
        $user = $request->user();

        $data = $request->only(['email', 'username', 'password']);

        $user = $users->update($user->id, $data);

        return new UserResource($user);
    }
}

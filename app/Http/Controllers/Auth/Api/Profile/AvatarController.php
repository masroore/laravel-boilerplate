<?php

namespace App\Http\Controllers\Auth\Api\Profile;

use App\Events\User\ChangedAvatar;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\User\UploadAvatarRawRequest;
use App\Http\Resources\UserResource;
use App\Repositories\User\UserRepository;
use App\Services\Upload\UserAvatarManager;
use Illuminate\Http\Request;

class AvatarController extends ApiController
{
    private UserRepository $users;

    /**
     * @var UserAvatarManager
     */
    private $avatarManager;

    public function __construct(UserRepository $users, UserAvatarManager $avatarManager)
    {
        $this->users = $users;
        $this->avatarManager = $avatarManager;
    }

    /**
     * @return UserResource
     */
    public function update(UploadAvatarRawRequest $request)
    {
        $name = $this->avatarManager->uploadAndCropAvatar(
            $request->file('file')
        );

        $user = $this->users->update(
            auth()->id(),
            ['avatar' => $name]
        );

        event(new ChangedAvatar());

        return new UserResource($user);
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return UserResource
     */
    public function updateExternal(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|url',
        ]);

        $this->avatarManager->deleteAvatarIfUploaded(
            auth()->user()
        );

        $user = $this->users->update(
            auth()->id(),
            ['avatar' => $request->url]
        );

        event(new ChangedAvatar());

        return new UserResource($user);
    }

    /**
     * Remove avatar for currently authenticated user and set it to null.
     *
     * @return UserResource
     */
    public function destroy()
    {
        $user = auth()->user();

        $this->avatarManager->deleteAvatarIfUploaded($user);

        $user = $this->users->update(
            $user->id,
            ['avatar' => null]
        );

        event(new ChangedAvatar());

        return new UserResource($user);
    }
}
